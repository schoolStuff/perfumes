'''Yun Li
Doc 08, 2016'''

#import csv
import unicodecsv as csv
import itertools
import json

file_perfume = './perfume/perfumes.json'
file_notes = './perfume/notes.json'

# list of notes
with open(file_notes, 'r') as f:
    notes = [a['name'] for a in json.load(f)]

#def get_note_network(yr1, yr2):
# network of notes form 1950s
note_pair = []
with open(file_perfume, 'r') as f:
    entries = [a['notes'] for a in json.load(f) if a['year']<=1959 and a['year']>=1950]
for entry in entries:
    for a in itertools.combinations(entry, 2):
        note_pair.append(a)

with open('1950.csv', 'wb') as f:
    notewriter = csv.writer(f, delimiter=',')
    for a in note_pair:
        notewriter.writerow([a[0], a[1]])

