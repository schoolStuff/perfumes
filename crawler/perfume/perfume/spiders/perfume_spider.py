'''Yun Li
Nov 30, 2016'''

import scrapy
from scipy.stats import mode

class PerfumeSpider(scrapy.Spider):
    name = 'perfume'
    start_urls = ['http://www.fragrantica.com/notes/']

    def parse(self, response):
        # follow links to notes pages
        for href in response.css('div.notebox a::attr(href)').extract():
            yield scrapy.Request(response.urljoin(href), callback=self.parse_notes)

    def parse_notes(self, response):
        # follow links to perfume pages
        for href_perfume in response.xpath('//div[@id="col1"]/a[@href]/@href').extract():
            yield scrapy.Request(response.urljoin(href_perfume), \
                    callback=self.parse_perfumes)

    def parse_perfumes(self, response):
        def get_year():
            try:
                li = response.css('div p').re('\D\d{4}\D')
                li = [int(y[1:-1]) for y in li if int(y[1:-1]) > 1800 and\
                        int(y[1:-1]) < 2017]
                val, count = mode(li)
                print val
                year = int(max(val))
            except:
                year = None
            return year

        yield {
                'name': response.css('h1 span::text').re('^(.*?) for.*'),
                'gender': response.css('h1 span::text').re('for.*'),
                'designer': response.css('span span a span::text').extract(),
                'notes': response.css('div p span.rtgNote img::attr(title)').extract(),
                'year': get_year(),
                }




