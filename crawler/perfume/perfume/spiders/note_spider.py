'''Yun Li
Dec 08, 2016'''

import scrapy

class noteSpider(scrapy.Spider):
    name = 'note'
    start_urls = ['http://www.fragrantica.com/notes/']

    def parse(self, response):
        # follow links to notes pages
        for notes in response.css('div.notebox a::text').extract():
            yield {'name': notes}
