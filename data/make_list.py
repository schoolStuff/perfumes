# -*- coding: utf-8 -*-
'''Yun Li
Doc 08, 2016'''

import csv
import json

file_perfume = './perfume/perfumes.json'
file_notes = './perfume/notes.json'

# make list of notes
with open(file_notes, 'r') as f:
    data = json.load(f)

notes = [a['name'] for a in data]

with open('list_notes.csv', 'w') as f:
    noteswriter = csv.writer(f, delimiter=',')
    for n in notes:
        noteswriter.writerow([n])

