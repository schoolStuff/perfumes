import json 
import csv
from itertools import combinations
from collections import defaultdict

if __name__ == '__main__':
    scents = defaultdict(lambda: [])

    with open('perfumes.json') as f:
        rows = json.load(f)
        for row in rows:
            if not row['year']:
                continue
            perfume_name = row['name'][0]
            perfume_name = perfume_name.encode('utf-8')
            for scent in row['notes']:
                scents[scent].append(perfume_name)
            
    with open('perfumes.csv', 'w') as f:
        output = csv.writer(f, delimiter=',')
        for scent in scents.keys():
            l = len(scents[scent])
            print(scent, l * (l-1) / 2)
            for combination in combinations(scents[scent], 2):
                ...
                #output.writerow(combination)
